<?php
/**
 * @file
 * pushtape_events.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pushtape_events_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:shows
  $menu_links['main-menu:shows'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'shows',
    'router_path' => 'shows',
    'link_title' => 'Shows',
    'options' => array(
      'attributes' => array(
        'title' => 'Shows',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Shows');


  return $menu_links;
}
